package main

import (
	"context"
	"fmt"
	karma "github.com/reconquest/karma-go"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type User struct {
	Username string `json:"username" bson:"_username"`
	Message  string `json:"message" bson:"_message"`
	ID       int    `json:"user_id" bson:"user_id"`
}

func connectToDatabase(message string, username string, ID int) error {

	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		karma.Format(
			err,
			"unable to connect to DB",
		)
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		karma.Format(
			err,
			"unable to check connection",
		)
	}

	log.Printf("Connected to MongoDB!")
	collectionName := client.Database("TelegramBotUsers").Collection("userData")

	userData := User{
		Username: username,
		Message:  message,
		ID:       ID,
	}

	insertResult, err := collectionName.InsertOne(context.TODO(), userData)
	if err != nil {
		karma.Format(
			err,
			"Unable to insert document.",
		)
	}

	log.Printf("Inserted a single document: ", insertResult.InsertedID)

	err = client.Disconnect(context.TODO())
	if err != nil {
		karma.Format(
			err,
			"Unable to close connetcion to mongoDB.",
		)
	}
	return nil

}

func getNumOfUsers() int {
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		karma.Format(
			err,
			"unable to connect to DB",
		)
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		karma.Format(
			err,
			"unable to check connection",
		)
	}
	log.Printf("Connected to MongoDB for get numbers of users")
	collectionName := client.Database("TelegramBotUsers").Collection("userData")

	findResult, err := collectionName.Distinct(context.TODO(), "user_id",
		bson.D{{}})
	if err != nil {
		karma.Format(
			err,
			"unable to find number of ID",
		)
	}

	fmt.Println("findResult", findResult)
	fmt.Println(len(findResult))
	return (len(findResult))

}

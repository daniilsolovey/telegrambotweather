package main

import (
	"encoding/json"
	"errors"
	"github.com/reconquest/karma-go"
	"net/http"
	"strings"
)

type featureMember struct {
	GeoObject struct {
		Point struct {
			Pos string `json:"pos"`
		} `json:"Point"`
	} `json:"GeoObject"`
}
type geoData struct {
	Response struct {
		GeoObjectCollection struct {
			FeatureMember []featureMember `json:"featureMember"`
		} `json:"GeoObjectCollection"`
	} `json:"response"`
}
type weatherData struct {
	Fact struct {
		Temp int `json:"temp"`
	} `json:"fact"`
}
type apiData struct {
	apiKey string
}

func getlocation(city string, config *Config) (geoData, error) {
	locationAPI := config.LocationAPI
	geoURL := "https://geocode-maps.yandex.ru/1.x/?format=json&apikey=" +
		locationAPI + "&geocode=" + city
	georequest, err := http.NewRequest("GET", geoURL, nil)
	if err != nil {
		return geoData{}, karma.Format(
			err,
			"unable to get request to geoURL",
		)
	}
	geoclient := &http.Client{}
	georesp, err := geoclient.Do(georequest)
	if err != nil {
		return geoData{}, karma.Format(
			err,
			"unable to send an HTTP request",
		)
	}
	defer georesp.Body.Close()
	var coordinates geoData
	if err := json.NewDecoder(georesp.Body).Decode(&coordinates); err != nil {
		return geoData{}, err
	}

	return coordinates, nil
}
func convertCoordinates(location geoData) (string, string, error) {
	if len(location.Response.GeoObjectCollection.FeatureMember) == 0 {
		err := errors.New("geodata is nil")
		return "", "", err
	}
	convertLocation := strings.Fields(
		location.Response.GeoObjectCollection.FeatureMember[0].GeoObject.Point.Pos,
	)

	lat := convertLocation[1]
	lon := convertLocation[0]

	return lat, lon, nil
}
func getData(
	lat string, lon string, config *Config,
) (weatherData, error) {
	lat = "lat=" + lat
	lon = "lon=" + lon
	coordinatesURL := lat + "&" + lon
	URL := "http://api.weather.yandex.ru/v1/informers?"
	weatherURL := URL + coordinatesURL

	request, err := http.NewRequest("GET", weatherURL, nil)
	if err != nil {
		return weatherData{}, err
	}
	request.Header.Set("X-Yandex-API-Key", config.WeatherAPI)
	client := &http.Client{}
	resp, err := client.Do(request)
	if err != nil {
		return weatherData{}, err
	}
	defer resp.Body.Close()

	var weather weatherData
	if err := json.NewDecoder(resp.Body).Decode(&weather); err != nil {
		return weatherData{}, err
	}
	return weather, nil
}

func findWeather(config *Config, city string) (weatherTemp int, error error) {
	location, err := getlocation(city, config)
	if err != nil {
		return 0, karma.Format(
			err,
			"unable to get location data",
		)
	}
	lat, lon, err := convertCoordinates(location)
	if err != nil {
		return 0, karma.Format(
			err,
			"unable to get location data",
		)
	}
	weather, err := getData(lat, lon, config)
	if err != nil {
		return 0, karma.Format(
			err,
			"unable to get temperature data",
		)
	}
	return weather.Fact.Temp, nil

}

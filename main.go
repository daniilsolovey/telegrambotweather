package main

import (
	"github.com/go-telegram-bot-api/telegram-bot-api"
	karma "github.com/reconquest/karma-go"
	"log"
	"strconv"
	"strings"
)

func main() {
	config, err := LoadConfig("config.toml")
	if err != nil {
		log.Printf("unable to read config file")
	}
	CreateBot(config)
}

func IsLetter(s string) bool {
	for _, r := range s {
		if (r < 'a' || r > 'z') && (r < 'A' || r > 'Z') && r != '/' && r != ' ' {
			return false
		}
	}
	return true
}

func CreateBot(config *Config) error {
	bot, err := tgbotapi.NewBotAPI(config.TelegramBotToken)
	if err != nil {
		return err
	}

	log.Printf("Authorized on account %s", bot.Self.UserName)
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)
	for update := range updates {
		if update.Message == nil {
			continue
		}
		if IsLetter(update.Message.Text) == true && update.Message.Text != "" {
			switch update.Message.Text {
			case "/start":
				msg := tgbotapi.NewMessage(update.Message.Chat.ID,
					config.StartMessage)
				bot.Send(msg)
			case "/users":
				numUsers := getNumOfUsers()
				numUsersString := strconv.Itoa(numUsers)
				msg := tgbotapi.NewMessage(update.Message.Chat.ID,
					numUsersString)
				bot.Send(msg)
			default:
				text := update.Message.Text
				tableMessage := strings.Fields(text)
				if len(tableMessage) == 1 {
					message, err := findWeather(config, update.Message.Text)
					if err != nil {
						errormsg := "Input correct name of city"
						msg := tgbotapi.NewMessage(update.Message.Chat.ID,
							errormsg)
						bot.Send(msg)
						break
					}
					From := update.Message.From
					Username := From.UserName
					ID := From.ID

					err = connectToDatabase(update.Message.Text, Username, ID)
					if err != nil {
						karma.Format(
							err,
							"Database error",
						)
					}
					messageString := strconv.Itoa(message)
					messageSend := "Temperature in " + update.Message.Text +
						" = " +
						messageString + " degrees celsius"
					msg := tgbotapi.NewMessage(update.Message.Chat.ID,
						messageSend)
					bot.Send(msg)
				} else {
					for _, s := range tableMessage {
						message, err := findWeather(config, s)
						if err != nil {
							errormsg := "Input correct name of city"
							msg := tgbotapi.NewMessage(update.Message.Chat.ID,
								errormsg)
							bot.Send(msg)
						}

						messageString := strconv.Itoa(message)
						messageSend := "Temperature in " + s + " = " +
							messageString +
							" degrees celsius"
						msg := tgbotapi.NewMessage(update.Message.Chat.ID,
							messageSend)
						bot.Send(msg)
					}
					From := update.Message.From
					Username := From.UserName
					ID := From.ID
					err = connectToDatabase(update.Message.Text, Username, ID)
					if err != nil {
						karma.Format(
							err,
							"Database error",
						)
					}
				}
			}
		} else {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID,
				"Use the correct words for search temperature,(don't use numbers or special symbols)")
			bot.Send(msg)
		}
	}
	return nil
}

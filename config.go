package main

import (
	"github.com/kovetskiy/ko"
)

type Config struct {
	TelegramBotToken string `toml:"telegrambot_token"`
	LocationAPI      string `toml:"location_api"`
	WeatherAPI       string `toml:"weather_api"`
	StartMessage     string `toml:"start_message"`
}

func LoadConfig(path string) (*Config, error) {
	config := &Config{}
	err := ko.Load(path, config)
	if err != nil {
		return nil, err
	}

	return config, nil
}
